package com.dt.platform.proxy;

public class ServiceNames {
	
	/**
	 * 资产相关的服务 <br>
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String EAM="service-eam";
	
	/**
	 * 人事系统相关的服务 <br>
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String HRM="service-hrm";

	/**
	 * 数据中心相关的服务 <br>
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String DATACENTER="service-datacenter";

	/**
	 * 运维服务 <br>
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String OPS="service-ops";

	/**
	 * 功能服务 <br>
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String COMMON="service-common";


	/**
	 * 知识库服务 <br>
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String KNOWLEDGEBASE="service-knowledgebase";

	/**
	 * 合同服务 <br>
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String CONTRACT="service-contract";

	/**
	 * 车辆服务 <br>
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String VEHICLE="service-vehicle";

	/**
	 * CMS服务 <br>
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String CMS="service-cms";

	/**
	 * 数据中心相关的服务 <br>
	 * 对应 bootstrap.yml 中 spring.application.name 属性
	 * */
	public static final String MOBILE="service-mobile";
}
